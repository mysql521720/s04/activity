SELECT * FROM artists WHERE name LIKE "%d%";

SELECT * FROM songs WHERE length<250;

SELECT songs.song_name, albums.album_title, songs.length FROM songs
	JOIN albums ON songs.album_id = albums.id;

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	WHERE albums.album_title LIKE "%a%";

SELECT * FROM albums ORDER BY album_title DESC LIMIT 5;

SELECT * FROM albums
	JOIN songs ON songs.album_id = albums.id
	ORDER BY album_title DESC;